extends Area2D

class_name EnemyProjectile

export var damage:int = 2
var velocity:Vector2
export var rotationRate:float = 0.0

func _ready():
	self.connect("body_entered", self, "_on_Area2D_body_entered")

func _on_Area2D_body_entered(body:PhysicsBody2D):
	var player:Player = body as Player
	if player != null:
		player.hit(damage)

func _physics_process(delta: float) -> void:
	position += velocity * delta
	rotate(rotationRate * delta)