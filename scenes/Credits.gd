extends Node

const music = preload("res://music/credits.ogg")

# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	AudioPlayer.playMusic(music)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	if Input.is_action_just_pressed("ui_accept"):
		set_process(false)
		AudioPlayer.playSound(Sounds.sweepUp)
		yield(ScreenTransitioner.transitionOut(1.0, ScreenTransitioner.DIAMONDS), "completed")
		yield(get_tree().create_timer(0.5), "timeout")
		var mainScene:PackedScene = load("res://scenes/credits-2.tscn")
		SceneManager.changeScene(mainScene)
		AudioPlayer.playSound(Sounds.sweepDown)
		yield(ScreenTransitioner.transitionIn(1.0, ScreenTransitioner.DIAMONDS), "completed")
