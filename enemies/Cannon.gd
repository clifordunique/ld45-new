extends Enemy

const DELAY:float = 1.5
const SPEED:float = 80.0
const cannonball:PackedScene = preload("cannonball.tscn")
const sound:AudioStream = preload("cannon.wav")
const MAX_DIST:float = 150.0

var active:bool = false

const DICT:Dictionary = {
	"e": Vector2(1, 0),
	"ne": Vector2(1, -1),
	"n": Vector2(0, -1),
	"nw": Vector2(-1, -1),
	"w": Vector2(-1, 0),
}

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	Events.connect("screen_transitioned", self, "_onScreenTransitioned")
	_startIt()

func _startIt() -> void:
	while true:
		yield(get_tree().create_timer(DELAY), "timeout")
		if active && !get_tree().paused:

			var player:Player = Globals.player
			if player == null:
				continue

			if global_position.distance_to(player.global_position) > MAX_DIST:
				continue

			var dir:Vector2 = DICT[$AnimatedSprite.animation]
			var instance:EnemyProjectile = cannonball.instance()
			instance.velocity = dir.normalized() * SPEED
			get_parent().add_child(instance)
			instance.global_position = global_position + dir * 15
			Node2DShaker.shake(SceneManager.currentScene, .2, 1)
			AudioPlayer.playSound(sound)

func _physics_process(delta: float) -> void:
	var player:Player = Globals.player
	if player == null:
		return

	var dir:Vector2 = player.global_position - global_position
	var best:float = 999
	var result:String
	for key in DICT:
		var angleTo:float = abs(dir.angle_to((DICT[key])))
		if angleTo < best:
			best = angleTo
			result = key

	$AnimatedSprite.play(result)

func _onScreenTransitioned(screenName:String) -> void:
	active = Level.isInScreen(self, screenName)
