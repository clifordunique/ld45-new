extends KinematicBody2D

class_name MetalMan

# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"

const hitSound = preload("res://sfx/enemy-hit.wav")
export var deathExplosion:PackedScene
const sawSound:AudioStream = preload("res://sfx/saw.wav")
const smallSawSound:AudioStream = preload("res://sfx/sawsmall.wav")

const deathSound:AudioStream = preload("res://player/death.wav")
var saw:PackedScene = preload("saw.tscn")
var sawSmall:PackedScene = preload("sawsmall.tscn")
var targetX:float;
var originalX:float;
const SMALL_SAW_SPEED_1:float = 132.0
const SMALL_SAW_SPEED_2:float = 100.0
const SAW_SPEED:float = 232.0

var health:int = 18
var hitTimer:int = 0
var hitFrames:int = 50
var dead:bool = false
var sawInstance
var remainder:float = 0

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	Globals.metalManActive = false
	$HitSprite.visible = false
	_jump()
	Events.emit_signal("metal_health_changed", health, 18)

func _jump() -> void:
	while !Globals.metalManActive:
		yield(get_tree(), "idle_frame")



	yield(get_tree().create_timer(0.5), "timeout")
	while true:
		targetX = rand_range(155.0, 175.0)
		originalX = position.x
		$Tween.interpolate_method(self, "_jumpTween", 0, 1, 0.75, Tween.TRANS_LINEAR, Tween.EASE_IN)
		$Tween.start()
		$AnimatedSprite.play("jump")
		yield(get_tree().create_timer(rand_range(0.6, 0.65)), "timeout")
		_throwSmall()
		yield($Tween, "tween_completed")
		$AnimatedSprite.play("idle")
		yield(get_tree().create_timer(.4), "timeout")

		if randf() - remainder < 0.4:
			$AnimatedSprite.play("charge")
			_throwBig()
			AudioPlayer.playSound(sawSound, 1.1)
			yield(get_tree().create_timer(0.75), "timeout")
			$AnimatedSprite.play("throw")
			if dead:
				return
			var player = Globals.player
			var dir:Vector2 = Vector2.DOWN
			if player != null:
				dir = player.global_position - (global_position + Vector2(0, -20))
			if is_instance_valid(sawInstance):
				sawInstance.velocity = dir.normalized() * SAW_SPEED
			yield(get_tree().create_timer(.9), "timeout")
			remainder = 0
		else:
			remainder += 0.2
			yield(get_tree().create_timer(.5), "timeout")


		targetX = rand_range(25.0, 45.0)
		originalX = position.x
		$Tween.interpolate_method(self, "_jumpTween", 0, 1, 0.75, Tween.TRANS_LINEAR, Tween.EASE_IN)
		$Tween.start()
		$AnimatedSprite.play("jump")
		yield(get_tree().create_timer(rand_range(0.6, 0.65)), "timeout")
		_throwSmall()
		yield($Tween, "tween_completed")
		$AnimatedSprite.play("idle")
		yield(get_tree().create_timer(.4), "timeout")

		if randf() - remainder < 0.4:
			$AnimatedSprite.play("charge")
			_throwBig()
			AudioPlayer.playSound(sawSound, 1.1)
			yield(get_tree().create_timer(0.75), "timeout")
			$AnimatedSprite.play("throw")
			if dead:
				return
			var player = Globals.player
			var dir:Vector2 = Vector2.DOWN
			if player != null:
				dir = player.global_position - (global_position + Vector2(0, -20))
			if is_instance_valid(sawInstance):
				sawInstance.velocity = dir.normalized() * SAW_SPEED
			yield(get_tree().create_timer(.9), "timeout")
			remainder = 0
		else:
			remainder += 0.2
			yield(get_tree().create_timer(.5), "timeout")

func _throwSmall() -> void:
	if dead:
		return
	var player = Globals.player
	if player == null:
		return

	var dir:Vector2 = player.global_position - global_position
	var instance = sawSmall.instance()
	get_parent().add_child(instance)
	instance.global_position = global_position - dir.normalized() * 2
	instance.velocity = dir.normalized() * SMALL_SAW_SPEED_1
	AudioPlayer.playSound(smallSawSound, 0.95)

	#if health < 7:
		#var dir2:Vector2 = dir.rotated(PI / 3.5)
		#var instance2 = sawSmall.instance()
		#get_parent().add_child(instance2)
		#instance2.global_position = global_position
		#instance2.velocity = dir2.normalized() * SMALL_SAW_SPEED_2
		#AudioPlayer.playSound(smallSawSound, 0.95)
		#var dir3:Vector2 = dir.rotated(-PI / 3.5)
		#var instance3 = sawSmall.instance()
		#get_parent().add_child(instance3)
		#instance3.global_position = global_position
		#instance3.velocity = dir3.normalized() * SMALL_SAW_SPEED_2
		#AudioPlayer.playSound(smallSawSound, 0.95)

func _throwBig() -> void:
	if dead:
		return
	var player = Globals.player
	if player == null:
		return

	sawInstance = saw.instance()
	get_parent().add_child(sawInstance)
	sawInstance.global_position = global_position + Vector2(0, -20)

func _jumpTween(progress:float) -> void:
	position.x = lerp(originalX, targetX, progress)
	position.y = (progress - 0.5) * (progress - 0.5) * 230 + 20

func _physics_process(delta: float) -> void:
	var player = Globals.player
	if player == null:
		return

	$AnimatedSprite.flip_h = player.global_position.x - global_position.x < 0
	$HitSprite.flip_h = $AnimatedSprite.flip_h

	if !Globals.metalManActive:
		return

	if OS.is_debug_build() && Input.is_action_just_pressed("ui_cancel"):
		die()

	if dead:
		return

	if hitTimer > 0:
		$AnimatedSprite.visible = hitTimer % 3 < 2
		$HitSprite.visible = hitTimer % 8 < 3
	else:
		$AnimatedSprite.visible = true
		$HitSprite.visible = false

	hitTimer -= 1

	for body in $Area2D.get_overlapping_bodies():
		var pla:Player = body as Player
		if pla != null:
			pla.hit(4)


func hit(damage:int) -> void:
	if !Globals.metalManActive:
		return

	if dead:
		return
	AudioPlayer.playSound(hitSound)

	if hitTimer > 0:
		return
	health -= damage
	hitTimer = hitFrames
	Events.emit_signal("metal_health_changed", health, 18)
	if health <= 0:
		die()

func die() -> void:
	if dead:
		return

	dead = true
	if sawInstance != null && is_instance_valid(sawInstance):
		sawInstance.queue_free()

	Events.emit_signal("metal_death")
	var explosion = deathExplosion.instance()
	AudioPlayer.playSound(deathSound, 1.15)
	get_parent().add_child(explosion);
	explosion.global_position = global_position;
	explosion.emitting = true
	$AnimatedSprite.visible = false
	queue_free()
