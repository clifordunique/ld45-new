extends Area2D

class_name Enemy

export var health = 5
export var damage = 1
export var Explosion:PackedScene

const hitSound = preload("res://sfx/enemy-hit.wav")
const invincHitSound = preload("res://sfx/invinchit.wav")

func hit(damage):
	if health < 0:
		AudioPlayer.playSound(invincHitSound, 1)
		return

	AudioPlayer.playSound(hitSound, 1.2)
	health -= 1
	if health <= 0:
		die()

func die():
	var explosion = Explosion.instance()
	get_parent().add_child(explosion)
	explosion.global_position = global_position;
	explosion.emitting = true;
	queue_free()

func _physics_process(delta: float) -> void:
	_doPhysicsProcess(delta)

func _doPhysicsProcess(delta: float):
	for body in get_overlapping_bodies():
		var player:Player = body as Player
		if player != null:
			player.hit(damage)

