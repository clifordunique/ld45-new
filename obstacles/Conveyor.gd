extends Area2D

const SPEED:float = 35.0

export var left:bool = false

func _ready() -> void:
	Events.connect("screen_transitioned", self, "_onScreenTransitioned")

func _onScreenTransitioned(screenName:String) -> void:
	set_physics_process(Level.isInScreen(self, screenName))

func _physics_process(delta: float) -> void:
	if Globals.conveyorsDisabled:
		return
	if Globals.screenTransitioning:
		return

	for body in get_overlapping_bodies():
		var player:Player = body as Player
		var speed = SPEED
		if Globals.metalManActive:
			speed *= 0.6
		if player != null:
			player.move_and_collide(Vector2(speed * delta * (-1 if left else 1), 0))