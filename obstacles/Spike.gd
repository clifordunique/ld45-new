extends Area2D

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	connect("body_entered", self, "_onArea2DBodyEntered")

func _onArea2DBodyEntered(body:PhysicsBody2D) -> void:
	var player:Player = body as Player
	if player != null:
		player.kill()